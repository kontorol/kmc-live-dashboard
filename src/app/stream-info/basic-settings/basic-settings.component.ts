import { Component, OnInit } from '@angular/core';
import { LiveEntryService } from "../../services/live-entry.service";
import { KontorolLiveStreamEntry } from "kontorol-ngx-client/api/types/KontorolLiveStreamEntry";

@Component({
  selector: 'basic-settings',
  templateUrl: 'basic-settings.component.html',
  styleUrls: ['basic-settings.component.scss']
})
export class BasicSettingsComponent implements OnInit {
  public _currentEntry: KontorolLiveStreamEntry;

  constructor(private _liveEntryService: LiveEntryService) { }

  ngOnInit() {
    this._liveEntryService.liveStream$.subscribe(response => {
      if (response) {
        this._currentEntry = response;
       }
    });
  }
}
