import { Pipe, PipeTransform } from '@angular/core';
import { KontorolMediaType } from "kontorol-ngx-client/api/types/KontorolMediaType";

@Pipe({
  name: 'entryType'
})
export class EntryTypePipe implements PipeTransform {

  transform(value: KontorolMediaType): string {
    let entryType: string = "";
    switch (value) {
      case KontorolMediaType.audio:
        entryType = "Audio";
        break;
      case KontorolMediaType.video:
        entryType = "Video";
        break;
      case KontorolMediaType.image:
        entryType = "Image";
        break;
      case KontorolMediaType.liveStreamFlash:
      case KontorolMediaType.liveStreamQuicktime:
      case KontorolMediaType.liveStreamRealMedia:
      case KontorolMediaType.liveStreamWindowsMedia:
        entryType = "Live";
        break;
      default:
        entryType = "Unknown";
        break;
    }

    return entryType;
  }

}
