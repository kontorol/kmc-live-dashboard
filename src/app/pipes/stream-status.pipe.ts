import { Pipe, PipeTransform } from '@angular/core';
import { KontorolEntryServerNodeStatus } from "kontorol-ngx-client/api/types/KontorolEntryServerNodeStatus";
import { KontorolViewMode } from "kontorol-ngx-client/api/types/KontorolViewMode";

@Pipe({
  name: 'streamStatus'
})
export class StreamStatusPipe implements PipeTransform {

  transform(entryServerNodeStatus: KontorolEntryServerNodeStatus, viewMode = KontorolViewMode.allowAll): 'Live' | 'Initializing' | 'Offline' | 'Preview' {
    switch (entryServerNodeStatus) {
      case KontorolEntryServerNodeStatus.authenticated:
      case KontorolEntryServerNodeStatus.broadcasting:
        return 'Initializing';
      case KontorolEntryServerNodeStatus.playable:
        return (viewMode === KontorolViewMode.preview) ? 'Preview' : 'Live';
      case KontorolEntryServerNodeStatus.stopped:
      default:
        return 'Offline'
    }
  }

}
