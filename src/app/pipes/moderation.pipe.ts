import { Pipe, PipeTransform } from '@angular/core';
import { KontorolEntryModerationStatus } from "kontorol-ngx-client/api/types/KontorolEntryModerationStatus";

@Pipe({
  name: 'moderation'
})
export class ModerationPipe implements PipeTransform {

  transform(value: KontorolEntryModerationStatus): string {
    let moderationStatus: string = "";
    if (value) {
      switch (value) {
        case KontorolEntryModerationStatus.approved:
          moderationStatus = "Approved";
          break;
        case KontorolEntryModerationStatus.autoApproved:
          moderationStatus = "Auto Approved";
          break;
        case KontorolEntryModerationStatus.flaggedForReview:
          moderationStatus = "Flagged";
          break;
        case KontorolEntryModerationStatus.pendingModeration:
          moderationStatus = "Pending";
          break;
        case KontorolEntryModerationStatus.rejected:
          moderationStatus = "Rejected";
          break;
      }
      return moderationStatus;
    }
  }

}
