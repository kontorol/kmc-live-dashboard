import { Pipe, PipeTransform } from '@angular/core';
import { KontorolRecordStatus } from "kontorol-ngx-client/api/types/KontorolRecordStatus";

@Pipe({
  name: 'recordingType'
})
export class RecordingTypePipe implements PipeTransform {

  transform(value: KontorolRecordStatus): string {
    switch (value) {
      case KontorolRecordStatus.disabled:
        return "";
      case KontorolRecordStatus.appended:
        return 'appendRecording';
      case KontorolRecordStatus.perSession:
        return 'newEntryPerSession';
    }
  }

}
