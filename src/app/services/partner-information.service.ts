import { Injectable } from '@angular/core';
import { KontorolClient } from "kontorol-ngx-client";
import { Observable } from "rxjs";

import { ConversionProfileListAction } from "kontorol-ngx-client/api/types/ConversionProfileListAction";
import { KontorolConversionProfileFilter } from "kontorol-ngx-client/api/types/KontorolConversionProfileFilter";
import { KontorolConversionProfileType } from "kontorol-ngx-client/api/types/KontorolConversionProfileType";
import { KontorolConversionProfileListResponse } from "kontorol-ngx-client/api/types/KontorolConversionProfileListResponse";
import { ConversionProfileAssetParamsListAction } from "kontorol-ngx-client/api/types/ConversionProfileAssetParamsListAction";
import { KontorolConversionProfileAssetParamsFilter } from "kontorol-ngx-client/api/types/KontorolConversionProfileAssetParamsFilter";
import { KontorolConversionProfileAssetParamsListResponse } from "kontorol-ngx-client/api/types/KontorolConversionProfileAssetParamsListResponse";
import { UiConfListTemplatesAction } from "kontorol-ngx-client/api/types/UiConfListTemplatesAction";
import { KontorolUiConfFilter } from "kontorol-ngx-client/api/types/KontorolUiConfFilter";
import { KontorolUiConfListResponse } from "kontorol-ngx-client/api/types/KontorolUiConfListResponse";
import { environment } from "../../environments/environment";

@Injectable()
export class PartnerInformationService {

  constructor(private _kontorolClient: KontorolClient) { }

  public getConversionProfiles(): Observable<KontorolConversionProfileListResponse> {
    return this._kontorolClient.request(new ConversionProfileListAction({
      filter: new KontorolConversionProfileFilter({
        typeEqual: KontorolConversionProfileType.liveStream
      })
    }));
  }

  public getConversionProfileFlavors(id: number): Observable<KontorolConversionProfileAssetParamsListResponse> {
    return this._kontorolClient.request(new ConversionProfileAssetParamsListAction({
      filter: new KontorolConversionProfileAssetParamsFilter({
        conversionProfileIdEqual: id
      })
    }));
  }

  public getUiconfIdByTag(): Observable<KontorolUiConfListResponse> {
    return this._kontorolClient.request(new UiConfListTemplatesAction({
      filter: new KontorolUiConfFilter({
        tagsMultiLikeOr: environment.bootstrap.uiConf_id_tag
      })
    }));
  }
}
